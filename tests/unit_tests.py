import unittest
import os, sys
sys.path.insert(0, os.path.abspath("../esco_skills_to_tax_skills"))
import paths

class MyTestCase(unittest.TestCase):
    def test_find_esco_occupation_id(self):
        escoskill_escooccupation_uri = {'oskadliggöra landminor': ['http://data.europa.eu/esco/occupation/fdc8adfb-1807-4146-98ae-3a26fbe19996'], 
                                        'genomföra avgränsning': ['http://data.europa.eu/esco/occupation/fdc8adfb-1807-4146-98ae-3a26fbe19996'], 
                                        'hitta landminor': ['http://data.europa.eu/esco/occupation/fdc8adfb-1807-4146-98ae-3a26fbe19996'], 
                                        'installera elmätare': ['http://data.europa.eu/esco/occupation/fddb7bd8-f176-4dc8-9d12-32b6072452c9'], 
                                        'tolka skriftlig kommunikation': ['http://data.europa.eu/esco/occupation/fde97584-d8e3-413e-a662-511c9055357e'], 
                                        'besvara förfrågningar i skriftlig form': ['http://data.europa.eu/esco/occupation/fde97584-d8e3-413e-a662-511c9055357e'], 
                                        'arbeta med e-tjänster som är tillgängliga för medborgarna': ['http://data.europa.eu/esco/occupation/fde97584-d8e3-413e-a662-511c9055357e'], 
                                        'skriva maskin snabbt': ['http://data.europa.eu/esco/occupation/fde97584-d8e3-413e-a662-511c9055357e'], 
                                        'chatta': ['http://data.europa.eu/esco/occupation/fde97584-d8e3-413e-a662-511c9055357e'], 
                                        'främja idrott i samband med folkhälsa': ['http://data.europa.eu/esco/occupation/fdf7b000-d923-4086-9660-eb8ee5e77ef0'], 
                                        'övervaka sändningskvalitet': ['http://data.europa.eu/esco/occupation/fe4a72af-6206-4d86-90c1-b5fb4b5ba2b2'], 
                                        'välja inspelningskälla': ['http://data.europa.eu/esco/occupation/fe4a72af-6206-4d86-90c1-b5fb4b5ba2b2'], 
                                        'förbereda sändningsutrustning': ['http://data.europa.eu/esco/occupation/fe4a72af-6206-4d86-90c1-b5fb4b5ba2b2'], 
                                        'övervaka luftkvalitet': ['http://data.europa.eu/esco/occupation/febd4100-9a9b-46a0-b964-8623d79d0f32'], 
                                        'framställa kosmetika': ['http://data.europa.eu/esco/occupation/ff5210a5-2ea9-43b4-bebc-8d565f928eaa'], 
                                        'förutsäga befolkningstrender': ['http://data.europa.eu/esco/occupation/ff656b3a-65a8-4b4f-b04e-9db12936519b'], 
                                        'lagra sorterat avfall': ['http://data.europa.eu/esco/occupation/ff8d4065-db44-47c7-a4bd-51b3c306ef70'], 
                                        'arkivera arbetsrelaterad dokumentation': ['http://data.europa.eu/esco/occupation/ffade2f4-2528-4dfd-b36a-6e4092be08ed']}
        
        escooccupation_uri_occupations_id = {'http://data.europa.eu/esco/occupation/a6007269-912a-4445-b792-b4d5fe016bb2': 'GBvP_Wah_zpr', 
                                             'http://data.europa.eu/esco/occupation/dc97adbe-f807-4ad8-8f3c-c24b3416cdef': 'mmci_acj_hYc', 
                                             'http://data.europa.eu/esco/occupation/c9862fdd-53b7-4da9-b742-4302d704836c': '97V6_Crw_yLm', 
                                             'http://data.europa.eu/esco/occupation/5979893a-0a30-447b-8f33-1ea405600583': 'toES_T9A_cSd', 
                                             'http://data.europa.eu/esco/occupation/2168e49d-e58a-43ec-a15e-8e173939b255': '4vox_7SW_ECf', 
                                             'http://data.europa.eu/esco/occupation/57e87533-e523-4ee5-bf21-94a39fcc279b': 'QMVw_bv8_366', 
                                             'http://data.europa.eu/esco/occupation/c9b2b0a7-0adc-4ca7-bf7c-dc95e18aeac7': 'uNjo_fz5_8Lw', 
                                             'http://data.europa.eu/esco/occupation/3b1ea27c-781c-41eb-821f-214285260dd2': '36VB_M84_Fri', 
                                             'http://data.europa.eu/esco/occupation/f0296b9c-764d-42ab-8de9-2a3729a9b67a': '58Ry_6Nx_Kaf', 
                                             'http://data.europa.eu/esco/occupation/22585d1e-48aa-45a6-a8c3-d072b4ff1b31': 'fLby_32F_Cp5', 
                                             'http://data.europa.eu/esco/occupation/1c5a45b9-440e-4726-b565-16a952abd341': 'NRb2_Dno_gXD', 
                                             'http://data.europa.eu/esco/occupation/1619d7f3-7d95-407c-a20e-47745bfcde73': 'nXbj_R18_X7R', 
                                             'http://data.europa.eu/esco/occupation/3b129d8d-268e-465a-b0c5-bf32cae59a41': 'R4Sb_jub_zy7', 
                                             'http://data.europa.eu/esco/occupation/557f9ccc-4e46-4bb2-8d20-272cd978b152': 'WS3U_1rG_H8G', 
                                             'http://data.europa.eu/esco/occupation/46d0568d-7415-4643-94aa-89d2470fbe3c': 'scJs_VZu_AWj', 
                                             'http://data.europa.eu/esco/occupation/82616669-cc82-4455-9332-a2df209e9f3e': '39H4_CzN_YiS', 
                                             'http://data.europa.eu/esco/occupation/20786e6d-36f0-4d1e-b033-05101fa71f3c': 'aEnA_yj5_Lpj', 
                                             'http://data.europa.eu/esco/occupation/e8fca903-6590-48a8-8902-094bb41ee90c': 'MdrQ_f7z_1Kd', 
                                             'http://data.europa.eu/esco/occupation/1c85eee2-a8ca-4750-a889-10a3e6c17b80': 'fieo_1CB_ZLb', 
                                             'http://data.europa.eu/esco/occupation/c467efc2-6b87-429d-b0be-44512cf9535d': 'YNbH_xsg_6cc', 
                                             'http://data.europa.eu/esco/occupation/73b10c97-b003-45dc-a36d-c1d585c04be1': 'rbYk_KYX_7G6', 
                                             'http://data.europa.eu/esco/occupation/094e04fd-3039-4615-850b-b232794f84f5': 'uMPB_saq_BA2', 
                                             'http://data.europa.eu/esco/occupation/2cf2b905-3308-4b5d-8e8d-633fc7a3f3ce': 'bXLE_XE4_nGB', 
                                             'http://data.europa.eu/esco/occupation/7f46db3a-754b-4bda-b287-5932fa062ee9': 'aPFC_13F_W6L', 
                                             'http://data.europa.eu/esco/occupation/a32187fa-bc37-4fc2-8e89-296632bfc271': 'jUQ1_RTv_5mc', 
                                             'http://data.europa.eu/esco/occupation/c44c37a2-88c3-4ac2-9cf9-959f42deb02b': 'AEzL_XhL_s5D', 
                                             'http://data.europa.eu/esco/occupation/5e748def-5b90-41e2-aa8b-2ebcc7d25ab0': 'uX6q_NN7_Yiy', 
                                             'http://data.europa.eu/esco/occupation/96e88e86-f2c7-42e6-a2b2-17e4f08d4ee0': 'GZGg_J6E_zPa',
                                             'http://data.europa.eu/esco/occupation/fdc8adfb-1807-4146-98ae-3a26fbe19996': 'SphY_FbH_u2a'}

        sample_esco_skill = "oskadliggöra landminor"
        sample_esco_occupation_uri ="http://data.europa.eu/esco/occupation/fdc8adfb-1807-4146-98ae-3a26fbe19996"
        sample_esco_occupation_id = "SphY_FbH_u2a"

        escooccupation_uri = escoskill_escooccupation_uri.get(sample_esco_skill)
        escooccupation_id = []
        for uri in escooccupation_uri:
            escooccupation_id.append(escooccupation_uri_occupations_id.get(uri))

        self.assertTrue(sample_esco_occupation_uri in escooccupation_uri)
        self.assertTrue(sample_esco_occupation_id in escooccupation_id)

    def test_paths(self):
        sample_esco_occupation_id = "1BgX_ivr_fea"
        sample_skill_id = "RCcY_Ccr_i1y"
        self.assertEqual(0, 0 if sample_skill_id in paths.esco_occupation_skill_map.get(sample_esco_occupation_id, set()) else 1)

    def test_skill_headline_penalty_cost(self):
        sample_penalized_broader = "HjPN_99i_cFD"
        skill_headline_penalty_weight = 0
        skill_headline_penalty_cost = skill_headline_penalty_weight * min(1, len(sample_penalized_broader))
        self.assertEqual(0, skill_headline_penalty_cost)

    def test_get_escoskill_escooccupation_uri(self):
        escoskill_escooccupation_uri = {'oskadliggöra landminor': ['http://data.europa.eu/esco/occupation/fdc8adfb-1807-4146-98ae-3a26fbe19996'], 
                                        'genomföra avgränsning': ['http://data.europa.eu/esco/occupation/fdc8adfb-1807-4146-98ae-3a26fbe19996'], 
                                        'hitta landminor': ['http://data.europa.eu/esco/occupation/fdc8adfb-1807-4146-98ae-3a26fbe19996'], 
                                        'installera elmätare': ['http://data.europa.eu/esco/occupation/fddb7bd8-f176-4dc8-9d12-32b6072452c9'], 
                                        'tolka skriftlig kommunikation': ['http://data.europa.eu/esco/occupation/fde97584-d8e3-413e-a662-511c9055357e'], 
                                        'besvara förfrågningar i skriftlig form': ['http://data.europa.eu/esco/occupation/fde97584-d8e3-413e-a662-511c9055357e'], 
                                        'arbeta med e-tjänster som är tillgängliga för medborgarna': ['http://data.europa.eu/esco/occupation/fde97584-d8e3-413e-a662-511c9055357e'], 
                                        'skriva maskin snabbt': ['http://data.europa.eu/esco/occupation/fde97584-d8e3-413e-a662-511c9055357e'], 
                                        'chatta': ['http://data.europa.eu/esco/occupation/fde97584-d8e3-413e-a662-511c9055357e'], 
                                        'främja idrott i samband med folkhälsa': ['http://data.europa.eu/esco/occupation/fdf7b000-d923-4086-9660-eb8ee5e77ef0'], 
                                        'övervaka sändningskvalitet': ['http://data.europa.eu/esco/occupation/fe4a72af-6206-4d86-90c1-b5fb4b5ba2b2'], 
                                        'välja inspelningskälla': ['http://data.europa.eu/esco/occupation/fe4a72af-6206-4d86-90c1-b5fb4b5ba2b2'], 
                                        'förbereda sändningsutrustning': ['http://data.europa.eu/esco/occupation/fe4a72af-6206-4d86-90c1-b5fb4b5ba2b2'], 
                                        'övervaka luftkvalitet': ['http://data.europa.eu/esco/occupation/febd4100-9a9b-46a0-b964-8623d79d0f32'], 
                                        'framställa kosmetika': ['http://data.europa.eu/esco/occupation/ff5210a5-2ea9-43b4-bebc-8d565f928eaa'], 
                                        'förutsäga befolkningstrender': ['http://data.europa.eu/esco/occupation/ff656b3a-65a8-4b4f-b04e-9db12936519b'], 
                                        'lagra sorterat avfall': ['http://data.europa.eu/esco/occupation/ff8d4065-db44-47c7-a4bd-51b3c306ef70'], 
                                        'arkivera arbetsrelaterad dokumentation': ['http://data.europa.eu/esco/occupation/ffade2f4-2528-4dfd-b36a-6e4092be08ed'],
                                        'simma': ['http://data.europa.eu/esco/occupation/3025dea4-1752-40a9-9431-fd874420386f', 
                                                  'http://data.europa.eu/esco/occupation/0d6bb2cc-06b0-481c-b553-2a973be03254']}
                
        escoskill = 'simma'
        escooccupation_uri_1 = 'http://data.europa.eu/esco/occupation/3025dea4-1752-40a9-9431-fd874420386f'
        escooccupation_uri_2 = 'http://data.europa.eu/esco/occupation/0d6bb2cc-06b0-481c-b553-2a973be03254'
        
        self.assertTrue(escooccupation_uri_1 in escoskill_escooccupation_uri[escoskill])
        self.assertTrue(escooccupation_uri_2 in escoskill_escooccupation_uri[escoskill])


if __name__ == '__main__':
    unittest.main()
