# ESCO Skills to Taxonomy Skills
This repository contains mapping ESCO Skills to Taxonomy Skills to help Redaction Team to easily find out which
ESCO Skills have a path to Taxonomy Skills and which one not.


## Set up environment

- Make sure python 3.11 is installed.
- Make sure pip is installed.
- Set up a venv (IntelliJ or similar environment):
    - File -> Project Structure -> SDKs -> + -> Add Python SDK... -> Virtual ENV Environment
- Install dependencies:
  ```pip install - r requirements.txt```

### Expected result:
```escoskill_skill_output.ods``` is the expected result when you run ```main.py```.
