from esco_taxonomy_BERT import concept_lookup
import jobtech_common
import pandas as pd
import paths

def esco_skill_to_tax_skill_SBERT():
    esco_to_skill = '../data/escoskill_taxskill.ods'
    esco_to_skill_BERT = pd.read_excel(esco_to_skill, engine='odf',
                                       usecols=['ESCO Skill', 'Tax Skill Id', 'Tax Skill Preferred Label', 'Distance'])
    esco_to_skill_BERT = jobtech_common.get_escoskill_taxskill_BERT(esco_to_skill_BERT)
    return esco_to_skill_BERT


def esco_skill_to_esco_occupation_uri():
    escoskill_escooccupation = '../data/Occupations_essential_skills_SV.csv'
    escoskill_escooccupation_uri = pd.read_csv(escoskill_escooccupation,
                                               usecols=[':esco-skill pref.label', ':esco-occupation-uri'])
    escoskill_escooccupation_uri = jobtech_common.get_escoskill_escooccupation_uri(escoskill_escooccupation_uri)
    return escoskill_escooccupation_uri


def esco_occupation_uri_to_esco_occupation_id():
    esco_occupation = "../data/esco-occupation.json"
    esco_uri_occupations_id = jobtech_common.get_taxonomy_concepts_json(esco_occupation)
    esco_uri_occupations_id = jobtech_common.get_escooccupation_uri_escooccupation_id(esco_uri_occupations_id)
    return esco_uri_occupations_id


esco_skill_to_tax_skill_BERT = esco_skill_to_tax_skill_SBERT()
escoskill_escooccupation_uri = esco_skill_to_esco_occupation_uri()
escooccupation_uri_occupations_id = esco_occupation_uri_to_esco_occupation_id()


def find_esco_occupation_id_list(esco_skill, escoskill_escooccupation_uri, escooccupation_uri_occupations_id):
    escooccupation_uri_list = escoskill_escooccupation_uri.get(esco_skill)
    if escooccupation_uri_list is not None:
        occupations_id_list = [escooccupation_uri_occupations_id.get(uri) for uri in escooccupation_uri_list]
        return occupations_id_list


skill_headlines_to_penalize = {"HjPN_99i_cFD", "s48H_Sft_DTn", "ihyv_sgw_vHu"}
path_weight = 1
skill_headline_penalty_weight = 0
lookup = {}
for (i, (key_BERT, candidates_BERT)) in enumerate(esco_skill_to_tax_skill_BERT.items()):
    if i % 100 == 0:
        print("Processed {:d}/{:d}".format(i, len(esco_skill_to_tax_skill_BERT)))
    esco_occupation_id_list = find_esco_occupation_id_list(key_BERT, escoskill_escooccupation_uri,
                                                 escooccupation_uri_occupations_id)

    if esco_occupation_id_list is not None:
        output_list_for_esco_skill = []
        for esco_occupation_id in esco_occupation_id_list:
            output_list_for_occupation_id = []
            for value_BERT in candidates_BERT:
                skill_id = value_BERT["id"]
                skill = concept_lookup[skill_id]
                penalized_broader = [x for x in skill["broader"] if x["id"] in skill_headlines_to_penalize]
                skill_headline_penalty_cost = skill_headline_penalty_weight * min(1, len(penalized_broader))

                path_cost = path_weight*(0 if skill_id in paths.esco_occupation_skill_map.get(esco_occupation_id, set()) else 1)

                output_list_for_occupation_id.append({
                    "esco_occupation_id": str(esco_occupation_id), "id": value_BERT["id"], "label": value_BERT["label"],
                    "distance": value_BERT["distance"], "path_cost": path_cost, "skill_headline_penalty_cost": skill_headline_penalty_cost,
                    "total_cost": value_BERT["distance"] + path_cost + skill_headline_penalty_cost
                })

            print("Candidate count at {:d} is {:d}".format(i, len(output_list_for_occupation_id)))
            output_list_for_occupation_id.sort(key=lambda x: x["total_cost"])
            output_list_for_esco_skill.append(output_list_for_occupation_id)
        output_list_for_esco_skill = jobtech_common.flatten_output(output_list_for_esco_skill)
        lookup[key_BERT] = output_list_for_esco_skill


output_list = [x for k, v in lookup.items() for x in v]

jobtech_common.render_table_output("../data/escoskill_skill_output.ods", lookup)
