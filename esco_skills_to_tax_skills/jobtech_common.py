from sentence_transformers import SentenceTransformer
from sklearn.neighbors import NearestNeighbors
from collections.abc import Iterable
from urllib.request import urlopen
import pyexcel_ods
import numpy as np
import time
import json

model = SentenceTransformer("KBLab/sentence-bert-swedish-cased")


def get_escoskill_taxskill_BERT(esco_to_skill_BERT):
    lookup = {}
    for item in esco_to_skill_BERT.values:
        key = item[0]
        if not(key in lookup):
            lookup[key] = []
        lookup[key].append({"id": item[1], "label": item[2], "distance": item[3]})
    return lookup


def get_escoskill_escooccupation_uri(esco_occupations):
    lookup = {}
    for item in esco_occupations.values:
        key = item[0]
        if not(key in lookup):
            lookup[key] = []
        lookup[key].append(item[1])
    return lookup


def get_escooccupation_uri_escooccupation_id(esco_occupations):
   return {item["esco_uri"]: item["id"] for item in esco_occupations["data"]["concepts"]}


def get_taxonomy_concepts_json(filename):
    with open(filename, encoding="utf8") as f:
        return json.load(f)


def get_taxonomy_concepts_url(url_tax):
    with urlopen(url_tax) as response:
        return response.read()


def flatten(l):
    for el in l:
        if isinstance(el, Iterable) and not isinstance(el, (str, bytes)):
            yield from flatten(el)
        else:
            yield el


def get_preferred_labels(concept):
    return [label for labels in [[concept["preferred_label"]]] for label in labels]


def get_lowercase_preferred_labels(tax_list):
    tax_labels = [get_preferred_labels(concept) for concept in tax_list]
    return [label.lower() for label in flatten(tax_labels)]


def concept_ids_dic(tax_labels, tax_list):
    dic_id = {}
    label_set = set(tax_labels)
    for concept in tax_list:
        for label in get_lowercase_preferred_labels([concept]):
            if label in label_set:
                dic_id[label] = concept["id"]
    return dic_id


def normalize(x):
    return x / np.linalg.norm(x)


def encode_vectors(labels_list):
    progress = ProgressReporter(len(labels_list))
    dst = []
    for label in labels_list:
        progress.end_of_iteration_message("Encoding...")
        dst.append(normalize(model.encode(label)))
    return dst


def nearest_neighbors(tax_labels, concepts_list):
    vectors = encode_vectors(tax_labels)
    neighbors = NearestNeighbors(n_neighbors=20).fit(vectors)
    distances, indices = neighbors.kneighbors(encode_vectors(concepts_list))
    print("distance shape is {:s}, indices shape is {:s}".format(str(distances.shape), str(indices.shape)))
    return indices, distances


def connected_concepts_dic(concepts_list, tax_labels,  indices, distances, concept_ids):
    result = {}
    for i, item in enumerate(concepts_list):
        output_list = []
        for (index, distance) in zip(indices[i, :], distances[i, :]):
            label = tax_labels[index]
            output_list.append({"id": concept_ids[label], "label": label, "distance": str(distance)})
        result[item] = output_list
    return result


def render_table(out_filename_ods, connected_concepts):
    dst = [["ESCO Skill", "Tax Skill Id", "Tax Skill Preferred Label", "Distance"]]
    for key in connected_concepts.keys():
        for values in connected_concepts[key]:
            dst.append([key, values["id"], values["label"], values["distance"]])
    pyexcel_ods.save_data(out_filename_ods, {None: dst})


def flatten_output(xss):
    return [x for xs in xss for x in xs]


def render_table_output(filename_ods, connected_concepts):
    dst = [["ESCO Skill", "ESCO Occupation Id", "Skill Id", "Skill Preferred Label", "Distance", "Path Cost",
            "Skill Headline Penalty Cost", "Total Cost"]]
    for key in connected_concepts.keys():
        for values in connected_concepts[key]:
            dst.append([key, values["esco_occupation_id"], values["id"], values["label"], values["distance"],
                        values["path_cost"], values["skill_headline_penalty_cost"], values["total_cost"]])
    pyexcel_ods.save_data(filename_ods, {None: dst})


def get_connected_concepts(skill, escoskill):
    skill_labels = get_lowercase_preferred_labels(skill)
    concept_ids = concept_ids_dic(skill_labels, skill)
    indices, distances = nearest_neighbors(skill_labels, escoskill)
    connected_concepts = connected_concepts_dic(escoskill, skill_labels, indices, distances, concept_ids)
    return connected_concepts


def compute_time_estimate(progress, elapsed_time):
    base = {"elapsed_time": elapsed_time,
            "progress": progress}
    if progress <= 0:
        return base
    time_per_progress = elapsed_time/progress
    remaining_progress = 1.0 - progress
    remaining_time = remaining_progress*time_per_progress
    return {**base,
            "time_per_progress": time_per_progress,
            "total_time": time_per_progress,
            "remaining_progress": remaining_progress,
            "remaining_time": remaining_time}


time_segmentation_seconds_breakdown = ["weeks", 7, "days", 24, "hours", 60, "minutes", 60, "seconds"]


def segment_time(t, segmentation=time_segmentation_seconds_breakdown):
    seg = list(reversed(segmentation))
    parts = []
    x = t
    while True:
        n = len(seg)
        unit = seg[0]
        if n == 1:
            parts.append((x, unit))
            break;
        else:
            d = seg[1]
            seg = seg[2:]
            xi = int(round(x))
            y = xi % d
            x = xi // d
            if 0 < y or (len(parts) == 0 and x == 0):
                parts.append((y, unit))
        if x == 0:
            break
    return parts


def format_time_segmentation(time_parts):
    n = min(2, len(time_parts))
    return ", ".join(map(lambda p: "{:d} {:s}".format(p[0], p[1]), reversed(time_parts[-n:])))


def format_seconds(seconds):
    return format_time_segmentation(segment_time(seconds))


def format_time_estimate(est):
    s = "Progress: {:d} %".format(int(round(100*est["progress"])))

    def timeinfo(k, lab):
        if k in est:
            return "\n{:s}: {:s}".format(lab, format_seconds(est[k]))
        return ""
    return s + timeinfo("total_time", "Total") + timeinfo("elapsed_time", "Elapsed") + timeinfo("remaining_time", "Remaining")


class ProgressReporter:
    def __init__(self, total_iterations, completed_iterations = 0, elapsed = 0):
        self.start = time.time() - elapsed
        self.total_iterations = total_iterations
        self.completed_iterations = completed_iterations
        self.rate_limiter = rate_limiter(1)

    def set_start(self, start):
        self.start = start

    def end_of_iteration(self):
        self.completed_iterations += 1

    def time_estimate(self):
        return compute_time_estimate(self.completed_iterations/self.total_iterations, time.time() - self.start)

    def elapsed(self):
        return time.time() - self.start

    def progress_report(self):
        return "Completed {:d} of {:d} iterations\n".format(
            self.completed_iterations, self.total_iterations) + format_time_estimate(self.time_estimate())

    def end_of_iteration_message(self, msg):
        self.end_of_iteration()
        if self.rate_limiter():
            print(msg)
            print(self.progress_report() + "\n")

    def to_data(self):
        return {"elapsed": self.elapsed(),
                "total_iterations": self.total_iterations,
                "completed_iterations": self.completed_iterations}


def rate_limiter(period=1):
    last = None

    def f():
        nonlocal last
        t = time.time()
        if (last is None) or (period + last) < t:
            last = t
            return True
        return False
    return f

