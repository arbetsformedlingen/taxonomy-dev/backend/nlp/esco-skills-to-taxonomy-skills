import jobtech_common
import json

tax_data = "https://data.jobtechdev.se/taxonomy/begrepp-och-vanliga-relationer.json"
tax = json.loads(jobtech_common.get_taxonomy_concepts_url(tax_data))
concepts = tax["data"]["concepts"]
concept_lookup = {x["id"]: x for x in concepts}


def esco_skill_to_tax_skill_SBERT():
    escoskill = [x for x in concepts if x["type"] in ["esco-skill"]]
    escoskill = jobtech_common.get_lowercase_preferred_labels(escoskill)
    skill = [x for x in concepts if x["type"] in ["skill"]]
    connected_concepts = jobtech_common.get_connected_concepts(skill, escoskill)
    return connected_concepts


def render_escoskill_taxskill_table():
    escoskill_taxskill = esco_skill_to_tax_skill_SBERT()
    jobtech_common.render_table("../data/escoskill_taxskill.ods", escoskill_taxskill)


render_escoskill_taxskill_table()










