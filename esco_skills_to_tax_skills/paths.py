import jobtech_common
import json
import os

## Load the data

taxonomy_filename = "https://data.jobtechdev.se/taxonomy/begrepp-och-vanliga-relationer.json"
concepts = json.loads(jobtech_common.get_taxonomy_concepts_url(taxonomy_filename))
concepts = concepts["data"]["concepts"]

#print(f"Loaded {len(concepts)} concepts")
    
## Make a map from every concept id to the concept and its neighbors

relations = ["broad_match", "narrow_match", "close_match", "exact_match", "broader", "related", "narrower"]

# This function creates a dictionary from the concept id to the concept
def make_concept_map(concepts):
    return {concept["id"]:concept.copy() for concept in concepts}

# This function adds a 'neighbor' key at every concept in a map.
def add_neighbors_to_concept_map(concept_map):
    for k, concept in concept_map.items():
        concept["neighbors"] = set()
        
    all_concepts = list(concept_map.values())
    for concept in all_concepts:
        a = concept["id"]
        for r in relations:
            for neighbor in concept[r]:
                b = neighbor["id"]
                concept["neighbors"].add(b)
                concept_map[a]["neighbors"].add(a)
    return concept_map

# Create a concept map with neighbors on the loaded data:
concept_map = add_neighbors_to_concept_map(make_concept_map(concepts))
    
## Find connected concepts

# This is a helper function for find_concept_paths, see below.
def find_concept_paths_sub(concept_map, visited_types, previous_path, concept_id, dst_concept_type, paths):
    concept_data = concept_map[concept_id]
    concept_type = concept_data["type"]
    path = previous_path + [concept_id]

    # If we reached a concept of the destination type, keep it
    if concept_type == dst_concept_type:
        paths.append(path)
        return

    # We don't want to visit the same concept type twice (I think)
    if concept_type in visited_types:
        return

    visited_types = visited_types.union({concept_type})
    
    # If we get here, loop over all the neighbors
    for neighbor_concept_id in concept_data["neighbors"]:
        find_concept_paths_sub(concept_map, visited_types, path, neighbor_concept_id, dst_concept_type, paths)

    visited_types.remove(concept_type)

# This function lists all paths with unique concept types from a given concept id
# to concepts of the given type.
def find_concept_paths(concept_map, src_id, dst_concept_type, ban_types=None):
    if ban_types is None:
        ban_types = {}
    paths = []
    visited_types = set().union(ban_types)
    find_concept_paths_sub(concept_map, visited_types, [], src_id, dst_concept_type, paths)
    return paths


## Demo

# This is an example of how you can use the above code.

esco_occupations = [k for (k, v) in concept_map.items() if v["type"] == "esco-occupation"]
#print("esco_occupations", esco_occupations)

def concept_label(cid):
    data = concept_map[cid]
    return "'{:s}' ({:s}, {:s})".format(data["preferred_label"], data["id"], data["type"])

esco_occupation_skill_map = {}

for esco_occupation_id in esco_occupations:
    skill_paths = find_concept_paths(concept_map, esco_occupation_id, "skill",
                                     ban_types={"isco-level-4", "occupation-field"})
    skill_ids = list({p[-1] for p in skill_paths})
    skill_count = len(skill_ids)

    show_debug_info = len(esco_occupation_skill_map) < 10
    
    if show_debug_info:
        #print("Esco occ: " + esco_occupation_id)
        #print("Esco uri: " + concept_map[esco_occupation_id]["esco_uri"])
        #print("Concept {:s} is related to {:d} skills".format(concept_map[esco_occupation_id]["preferred_label"], len(skill_ids)))
        if 0 < skill_count:
            path_count = len(skill_paths)        
            for i in range(min(30, path_count)):
                p = skill_paths[i]
                #print("   * " + " -> ".join([concept_label(x) for x in p]))
        
    esco_occupation_skill_map[esco_occupation_id] = skill_ids


#print("esco_occupation_skill_map:", esco_occupation_skill_map)


# Here is an example of how you can use the code to check if 
#sample_esco_occupation_id = "1BgX_ivr_fea"
#sample_skill_id = "RCcY_Ccr_i1y"
# print("\n\nIs the esco occupation {:s} connected with skill {:s}? {:s}".format(
#     sample_esco_occupation_id,
#     sample_skill_id,
#     "yes" if sample_skill_id in esco_occupation_skill_map[sample_esco_occupation_id] else "no"))
